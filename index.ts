var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongo = require("mongoose");
var session = require('express-session');
var jwt = require("jsonwebtoken");
var crypt = require('crypto');
var randtoken = require('rand-token');
mongo.set('useCreateIndex', true);
var Product = require(path.resolve('./src/mongoSchemes/product'));
var Cart = require(path.resolve('./src/mongoSchemes/cart'));
var User = require(path.resolve('./src/mongoSchemes/user'));
var Order = require(path.resolve('./src/mongoSchemes/order'))

var db = mongo.connect("mongodb://localhost:27017/AngularCRUD", function (err, response) {
    if (err) {
        console.log(err);
    }
    else {
        console.log('Connected to ' + db);
    }
});

var secret = "SUPER SECRET ahhahaha";

var app = express();

app.use(cors());
app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    secret: 'secret key',
    saveUninitialized: true
}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get("/api/getProducts/:page", function (request, response) {
    var page: number = request.params.page;
    var perPage: number = 2;
    Product
        .find({ isAvailable: true })
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec(function (error, data) {
            if (error) {
                response.send(error);
            }
            else {
                Product
                    .countDocuments({ isAvailable: true }, function (error, count) {
                        if (error) {
                            response.send(error);
                        }
                        else {
                            var countOfPages = Math.ceil(count / perPage);
                            response.send({ data: data, countOfPages: countOfPages });
                        }
                    });
            }
        });
});

app.get("/api/createData", function (request, response) {
    var bread = new Product({
        name: 'Bread',
        price: 1,
        isAvailable: true
    });
    var cheese = new Product({
        name: 'Cheese',
        price: 2,
        isAvailable: true
    });
    var egg = new Product({
        name: 'Egg',
        price: 3,
        isAvailable: true
    });
    var butter = new Product({
        name: 'Butter',
        price: 4,
        isAvailable: true
    });
    var sosages = new Product({
        name: 'Sosages',
        price: 5,
        isAvailable: true
    });
    Product.insertMany([bread, cheese, egg, butter, sosages], (err) => {
        if (err) {
            response.send(err);
        }
        else {
            response.send({ data: "Products have been Added to catalog!" });
        }
    });
});

app.delete("/api/clearData", (request, response) => {
    Product.deleteMany({}, function (err) {
        if (err) {
            response.send(err);
        }
        else {
            Cart.deleteMany({}, (err) => {
                if (err) {
                    response.send(err);
                }
                else {
                    Order.deleteMany({}, (err) => {
                        if (err) {
                            response.send(err);
                        }
                        else {
                            User.deleteMany({}, (err) => {
                                if (err) {
                                    response.send(err);
                                }
                                else {
                                    response.send({ data: "DATA has been cleared!" });
                                }
                            })
                        }
                    })
                }
            });
        }
    });
});

app.post('/api/addToCart', function (request, response) {
    const id = request.body.id;
    const token = request.headers.authorization;
    if (token) {
        jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
            if (err) {
                response.status(400).send(err)
            } else {
                Cart.exists({ product: id }, function (err, res) {
                    if (err) {
                        response.send(err);
                    }
                    if (res) {
                        Cart.find({ product: id }, function (err, res) {
                            Cart.updateOne({ product: id }, { $set: { count: ++res[0].count } }, function (err, raw) {
                                if (err) {
                                    response.send(err);
                                }
                            });
                        });
                    }
                    else {
                        Cart.insertMany(new Cart({
                            product: id,
                            count: 1
                        }), (err, doc) => {
                            if (err) {
                                response.send(err);
                            }
                        });
                    }
                });
                response.send({ data: "Has been added to cart!" });
            }
        })
    } else {
        response.status(400).send();
    }
});

app.delete('/api/cart', function (request, response) {
    Cart.deleteMany({}, function (err) {
        if (err) {
            response.send(err);
        }
        else {
            response.send();
        }
    });
})

app.post('/api/getOutOfCart', function (request, response) {
    Cart.findOne({ product: request.body.id }, function (err, res) {
        if (err) {
            response.send(err);
        }
        else {
            if (res.count > 1) {
                Cart.updateOne({ product: request.body.id }, { $set: { count: --res.count } }, function (err, raw) {
                    if (err) {
                        response.send(err);
                    }
                });
            }
            else {
                Cart.deleteOne({ product: request.body.id }, function (err) {
                    if (err) {
                        response.send(err);
                    }
                });
            }
        }
    });
    response.send({ data: "Has been got out of cart!" });
});

app.get("/api/cart", function (request, response) {
    Cart.find({}, { _id: 0, product: 1, count: 1 }).populate("product").exec((err, data) => {
        if (err) {
            response.send(err);
        } else {
            response.send(data);
        }
    })
});

app.post("/api/registration", function (request, response) {
    let { userEmail, userName, userCity, userPassword, isAdmin } = request.body.user;

    User.exists({ email: userEmail }, (err, res) => {
        if (err) {
            response.send(err)
        } else {
            if (res) {
                response.send({ message: 'Пользователь с таким электронным адресом уже зарегистрирован.' });
            } else {
                User.insertMany(new User({ email: userEmail, name: userName, city: userCity, password: crypt.createHash('sha256').update(userPassword).digest("hex"), isAdmin }), (err) => {
                    if (err) {
                        response.send(err);
                    } else {
                        response.send({ isSuccess: true, message: 'Пользователь успешно зарегистрирован.' });
                    }
                })
            }
        }
    })
});

app.post("/api/login", function (request, response) {
    const { userEmail, userPassword } = request.body.loginData;
    User.exists({ email: userEmail }, (err, res) => {
        if (err) {
            response.send(err);
        } else {
            if (res) {
                User.findOne({ email: userEmail }, (err, res) => {
                    if (err) {
                        response.send(err);
                    } else {
                        if (res.password == crypt.createHash('sha256').update(userPassword).digest("hex")) {
                            // шифрование
                            const token = jwt.sign({ id: res._id, email: res.email, password: res.password }, secret, { expiresIn: 100 });
                            const refreshToken = randtoken.uid(256)
                            response.send({ auth: true, token: token, refreshToken });
                        } else {
                            response.status(500).send('Неверный логин или пароль');
                        }
                    }
                })
            } else {
                response.status(500).send('Пользователь с таким электронным адресом не найден.');
            }
        }
    })
});

app.get("/api/isAdmin", function (request, response) {
    const token = request.headers.authorization;

    if (token) {
        jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
            if (err) {
                response.send({ isAdmin: false })
            } else {
                User.findById(decodedUser.id, (err, data) => {
                    if (err) {
                        response.send({ isAdmin: false })
                    } else {
                        if (data && data.isAdmin) {
                            response.send({ isAdmin: data.isAdmin })
                        } else {
                            response.send({ isAdmin: false })
                        }

                    }
                })
            }
        });
    } else {
        response.send({ isAdmin: false })
    }
})

app.get("/api/getUser", function (request, response) {
    const token = request.headers.authorization;

    if (token) {
        jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
            if (err) {
                response.status(400).send(err)
            } else {
                User.findById(decodedUser.id, (err, data) => {
                    if (err) {
                        response.send(err)
                    } else {
                        if (data) {
                            const { email, name, city, isAdmin } = data;
                            response.send({ email, name, city, isAdmin })
                        } else {
                            response.status(400).send(err)
                        }
                    }
                })
            }
        });
    } else {
        response.status(400).send();
    }
})

app.post('/api/addToCatalog', (request, response) => {
    const token = request.headers.authorization;

    if (token) {
        jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
            if (err) {
                response.send({ isAdmin: false })
            } else {
                Product.insertMany({ ...request.body.product, isAvailable: true }, (err, res) => {
                    if (err) {
                        response.send(err);
                    } else {
                        response.send();
                    }
                })
            }
        });
    } else {
        response.send({ isAdmin: false })
    }
});

app.post('/api/removeFromCatalog', (request, response) => {
    const token = request.headers.authorization;
    jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
        if (err) {
            response.status(400).send(err)
        } else {
            Product.updateOne(
                { _id: request.body.id },
                {
                    $set:
                    {
                        isAvailable: false
                    }
                }, (err) => {
                    if (err) {
                        response.send(err);
                    } else {
                        response.send();
                    }
                }
            )
        }
    });
});

app.post('/api/order', (request, response) => {
    const token: string = request.headers.authorization;
    jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
        if (err) {
            response.status(400).send(err)
        } else {
            let cart = request.body.cart.map((item) => {
                return { product: item.product._id, count: item.count };
            })
            Order.insertMany({ customer: decodedUser.id, products: cart, date: new Date() }, (err, doc) => {
                if (err) {
                    response.send(err)
                } else {
                    Cart.deleteMany({}, (err) => {
                        if (err) {
                            response.send(err);
                        } else {
                            response.send();
                        }
                    })
                }
            })
        }
    });
});

app.get('/api/orders', (request, response) => {
    const token: string = request.headers.authorization;
    jwt.verify(JSON.parse(token), secret, (err, decodedUser) => {
        if (err) {
            response.status(400).send(err)
        } else {
            Order.find({}, { _id: 0, __v: 0 })
                .populate("products.product")
                .populate("customer", "email")
                .exec((err, data) => {
                    if (err) {
                        response.send(err);
                    } else {
                        response.send(data);
                    }
                })
        }
    });
})

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});
