var mongo = require('mongoose');

var UserSchema = mongo.Schema({
    email: { type: String },
    name: { type: String },
    city: { type: String },
    password: { type: String },
    isAdmin: { type: Boolean },
}, { versionKey: false });


var User = mongo.model('User', UserSchema);

module.exports = User;