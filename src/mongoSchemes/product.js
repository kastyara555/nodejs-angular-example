var mongo = require('mongoose');

var productSchema = mongo.Schema({
    name: String,
    price: Number,
    isAvailable: Boolean
});

var Product = mongo.model('Product', productSchema);
 
module.exports = Product;