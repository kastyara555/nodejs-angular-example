var mongo = require('mongoose');

var orderSchema = mongo.Schema({
    customer: { type: mongo.Schema.Types.ObjectId, ref: 'User' },
    products: [{ product: {type: mongo.Schema.Types.ObjectId, ref: 'Product'}, count: Number }],
    date: Date
});

var Order = mongo.model('Order', orderSchema);
 
module.exports = Order;