var mongo = require('mongoose');

var cartSchema = mongo.Schema({
    product: { type: mongo.Schema.Types.ObjectId, ref: 'Product' },
    count: Number,
});

var Cart = mongo.model('Cart', cartSchema);
 
module.exports = Cart;