import { AuthService } from './services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private authService: AuthService, private router: Router) { }
  isLogined: Boolean;

  ngOnInit() {
    this.isLoginedIn();
  }

  logOut() {
    this.authService.logOut().subscribe(() => {
      this.router.navigate(['auth/login']).then(() => {
        this.isLoginedIn();
      })
    })
  }

  isLoginedIn() {
    this.isLogined = this.authService.getToken() ? true : false;
  }
}  
