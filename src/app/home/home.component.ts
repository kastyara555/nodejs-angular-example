import { AuthService } from './../services/auth.service';
import { commonResponse } from './../consts/models';
import { CommonService } from './../services/common.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService, private newService: CommonService, private router: Router) { }
  isAdmin: Boolean;
  user: any;
  orders = [];

  ngOnInit() {
    this.authService.getUser().subscribe(
      (user) => {
        this.user = user;
        this.newService.getOrders().subscribe(
          (data: Array<any>) => {
            this.orders = data;
            console.log(this.orders)
          }
        )
      }, () => {
        this.authService.logOut().subscribe(() => {
          this.router.navigate(['auth/login']).then(() => {
            location.reload();
          })
        })
      }
    )
    this.authService.isAdmin().subscribe((data: commonResponse) => {
      this.isAdmin = data.isAdmin;
      console.log(this.isAdmin);
    });
  }

  createData = () => this.newService.createData().subscribe((data: commonResponse) => { alert(data.data); }, error => alert(error))
  clearData = () => this.newService.clearData().subscribe((data: commonResponse) => { alert(data.data); }, error => alert(error))

  getSum = (order) => {
    let total = 0;
    for (let i = 0; i < order.products.length; i++) {
      total += order.products[i].product.price * order.products[i].count;
    }
    return total
  }
}
