export interface Page {
    number: Number,
    current: boolean
}