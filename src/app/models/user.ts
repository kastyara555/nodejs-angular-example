export interface User {
    email: string,
    name: string,
    city: string,
    password: string
}