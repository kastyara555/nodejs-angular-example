import { commonResponse } from './../consts/models';
import { AuthService } from './../services/auth.service';
import { CommonService } from './../services/common.service';
import { Component, OnInit } from '@angular/core';
import { Page } from '../models/page';
import { FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  constructor(private newService: CommonService, private auth: AuthService, private router: Router) { }
  repData = [];
  page: number = 1;
  countOnPage: number = 2;
  countOfPages: number = 0;
  pages: Page[] = [];
  isAdmin: Boolean;
  isLoading: Boolean = true;

  ngOnInit() {
    this.getProductsPage();
    this.auth.isAdmin().subscribe((data: commonResponse) => {
      this.isAdmin = data.isAdmin;
    })
  }

  addProductForm: FormGroup = new FormGroup({
    "name": new FormControl("", Validators.required),
    "price": new FormControl("", Validators.required),
  });

  getProductsPage = () => {
    this.newService.getProducts(this.page).subscribe((data: commonResponse) => {
      this.repData = data.data;
      this.countOfPages = data.countOfPages;
      this.pages = [];
      for (let i = 0; i < this.countOfPages; i++) {
        this.pages.push({
          number: i + 1,
          current: this.page == i + 1
        })
      }
      this.isLoading = false;
    });
  }

  onSubmit = (addForm: NgForm) => {
    this.newService.addToCatalog(addForm.value).subscribe((res) => {
      addForm.reset();
      this.getProductsPage();
    },
      () => {
        this.auth.logOut();
        this.router.navigate(['auth']).then(() => {
          location.reload();
        });
      })
  }

  addToCart = (id) => this.newService.addToCart(id).subscribe(
    (data: commonResponse) => { alert(data.data); },
    () => {
      this.auth.logOut();
      this.router.navigate(['auth']).then(() => {
        location.reload();
      });
    });

  removeFromCatalog = function (id) {
    this.newService.removeFromCatalog(id).subscribe(() => {
      this.page = 1;
      this.getProductsPage();
    },
      () => {
        this.auth.logOut();
        this.router.navigate(['auth']).then(() => {
          location.reload();
        });
      })
  }

  setPage = (page) => {
    this.page = page;
    this.getProductsPage();
  }

  prevPage = () => {
    this.page = this.page > 1 ? --this.page : this.page;
    this.getProductsPage();
  }

  nextPage = () => {
    this.page = this.page < this.countOfPages ? ++this.page : this.page;
    this.getProductsPage();
  }
}
