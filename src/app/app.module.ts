import { TokenInterceptor } from './interceptor/token-interceptor';
import { CommonService } from './services/common.service';
import { BrowserModule } from '@angular/platform-browser';  
import { NgModule } from '@angular/core';   
  
import { HttpModule } from '@angular/http';
  
import { AppComponent } from './app.component';
import { CatalogComponent } from './catalog/catalog.component';
import { CartComponent } from './cart/cart.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';
import { HomeComponent } from './home/home.component';  

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
  
  
@NgModule({  
  declarations: [  
    AppComponent,
    HomeComponent,
    CatalogComponent,
    CartComponent,
    NotFoundComponentComponent
  ],  
  imports: [  
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],  
  providers: [CommonService,
    {
      provide: HTTP_INTERCEPTORS,      
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]  
})  
export class AppModule { }  
