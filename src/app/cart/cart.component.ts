import { AuthService } from './../services/auth.service';
import { CommonService } from './../services/common.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private newService: CommonService, private auth: AuthService, private router: Router) { }
  cart = [];
  totalPrice: number = 0;
  isLoading: Boolean = true;

  ngOnInit() {
    this.getCart();
  }

  getCart = () => {
    this.newService.getCart().subscribe((data: Array<Object>) => {
      this.cart = [];
      this.cart = data;
      for (let i = 0; i < this.cart.length; i++) {
        this.totalPrice += this.cart[i].count * this.cart[i].product.price;
      }
      this.isLoading = false;
    });
  }

  editCart = (id, value) => {
    for (let i = 0; i < this.cart.length; i++) {
      if (this.cart[i].product._id == id) {
        this.cart[i].count += value;
        this.totalPrice += value * this.cart[i].product.price;
        if (this.cart[i].count == 0) {
          this.cart.splice(i, 1);
        }
      }
    }
  }

  addToCart = (id) => this.newService.addToCart(id).subscribe(
    () => this.editCart(id, 1),
    () => {
      this.auth.logOut();
      this.router.navigate(['auth']).then(() => {
        location.reload();
      });
    }
  );

  getOutOfCart = (id) => this.newService.getOutOfCart(id).subscribe(
    () => this.editCart(id, -1),
    () => {
      this.auth.logOut();
      this.router.navigate(['auth']).then(() => {
        location.reload();
      });
    }
  );

  order = () => {
    this.newService.order(this.cart).subscribe(
      () => {
        this.router.navigate(['/'])
      }
    )
  }
}
