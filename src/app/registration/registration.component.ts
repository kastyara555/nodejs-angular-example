import { commonResponse } from './../consts/models';
import { User } from './../models/user';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  constructor(private authService: AuthService, private router: Router) { }
  private user: User;

  registrationForm: FormGroup = new FormGroup({
    "userEmail": new FormControl("", [
      Validators.required,
      Validators.email
    ]),
    "userName": new FormControl("", Validators.required),
    "userCity": new FormControl("", Validators.required),
    "userPassword": new FormControl("", Validators.required),
    "isAdmin": new FormControl()
  });

  ngOnInit() {
  }

  onSubmit(registrationForm: NgForm){
    this.user = { ...registrationForm.value };
    console.log(registrationForm.value)
    this.authService.registration(this.user).subscribe((data: commonResponse) => {
      alert(data.message);
      if(data.isSuccess) {
        this.router.navigate(['auth/login']);
      }
    }, (err) => {
      console.log(err)
      alert(err.error.text)
    });
  }
}
