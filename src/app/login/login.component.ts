import { commonResponse } from './../consts/models';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }
  loginForm: FormGroup = new FormGroup({
    "userEmail": new FormControl("", [
      Validators.required,
      Validators.email
    ]),
    "userPassword": new FormControl("", Validators.required)
  });

  ngOnInit() {
  }

  goToRegistration() {
    this.router.navigate(['auth/registration'])
  }

  onSubmit(loginForm: NgForm) {
    const loginData = { ...loginForm.value };
    
    this.authService.login(loginData).subscribe((data: commonResponse) => { 
      if (data.token) {
        localStorage.setItem('currentUser', JSON.stringify(data.token));
        this.router.navigate(['']).then(() => {
          location.reload();
        });
      }
    }, (error) => {
      alert(error.error)
    });
  }

}
