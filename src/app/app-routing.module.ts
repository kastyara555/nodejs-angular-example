import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog/catalog.component';
import { CartComponent } from './cart/cart.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';
import { HomeComponent } from './home/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule } from './auth/auth.module';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'auth', loadChildren: () => AuthModule },
  { path: 'catalog', component: CatalogComponent},
  { path: 'cart', component: CartComponent},
  { path: '**', component: NotFoundComponentComponent }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
