import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  getToken(): string {
    return localStorage.getItem('currentUser');
  }

  getUser() {
    return this.http.get('http://localhost:8080/api/getUser/')
  }

  isAdmin() {
    return this.http.get('http://localhost:8080/api/isAdmin/')
  }

  registration(newUser: User) {
    return this.http.post('http://localhost:8080/api/registration/', { 'user': newUser })
  }

  login(loginData: any) {
    return this.http.post('http://localhost:8080/api/login/', { 'loginData': loginData })
  }

  logOut() {
    localStorage.removeItem('currentUser');
    console.log("remove cart");
    return this.http.delete('http://localhost:8080/api/cart/')
  }
}
