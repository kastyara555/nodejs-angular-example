import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
  
@Injectable()  
export class CommonService {  
  
  constructor(private http: HttpClient) { }  

  getProducts = (page) => {
    return this.http.get(`http://localhost:8080/api/getProducts/${page}`)
  }

  addToCatalog(product) {
    return this.http.post('http://localhost:8080/api/addToCatalog/',{'product': product})
  }

  removeFromCatalog(id) {
    return this.http.post('http://localhost:8080/api/removeFromCatalog/',{'id': id})
  }

  getProductsByIds = (ids) => {
    return this.http.post('http://localhost:8080/api/getProductsByIds/', { 'ids': ids })
  }

  createData = () => {
    return this.http.get('http://localhost:8080/api/createData/')
  }

  clearData = () => {
    return this.http.delete('http://localhost:8080/api/clearData/')
  }

  addToCart = (id) => {
    return this.http.post('http://localhost:8080/api/addToCart/',{'id': id})
  }

  getOutOfCart = (id) => {
    return this.http.post('http://localhost:8080/api/getOutOfCart/',{'id': id})
  }

  getCart = () => {
    return this.http.get('http://localhost:8080/api/cart/')
  }

  getOrders = () => {
    return this.http.get('http://localhost:8080/api/orders/')
  }

  order = (cart) => {
    return this.http.post('http://localhost:8080/api/order/', { 'cart': cart })
  }
}  
