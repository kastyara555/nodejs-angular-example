export interface commonResponse {
    isAdmin: Boolean,
    data: any,
    countOfPages: number,
    token: String,
    message: String,
    isSuccess: Boolean,
}